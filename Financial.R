# I. Assignment, 3. Theme: Financial

# 1.
library(readr)
library(MASS)
D = read.table("finance_data.csv", sep = ";", header = TRUE)
par(mfrow = c(1,1))
# example
hist(D$SLV, breaks=25, freq = FALSE)
par(mfrow = c(2,2))
hist(D$SLV, breaks=25, freq = FALSE)

###########################
########################### 
### Davids noter
plot(cumsum(D$SLV) , main = "Cumulative Sum", type='l') # Hvad der egentlig er af interesse
    # Konklusion: Efter 454 dage har vi oplevet totalt en 66 % stigning
acf(D$SLV, main = "ACF")
pacf(D$SLV, main = "PACF")
par(mfrow = c(1,1))

seqN = seq(-0.4,0.4,0.001)
hist(D$SLV, breaks = 30, freq=FALSE)
lines(seqN,dnorm(seqN,mean(D$SLV), sd(D$SLV)), col = 'green') # Normal
lines(seqN, dcauchy(seqN, mean(D$SLV), 0.035), col = 'blue') # Cauchy with arbitrary choice of paramaters
  # Looks quit normal
  # MANGLER: Find parameters using 2D-optimization of logL-function

logLcauchy = function(y,theta){
    -sum(dcauchy(y,location = theta[1], scale = theta[2],log=TRUE))
}

logLnorm = function(y,theta){
  -sum(dnorm(y, theta[1], theta[2],log=TRUE))
}

opt_cauchy <- nlminb(c(median(D$SLV),0.3), logLcauchy,lower=c(-Inf,0), y=D$SLV)
opt <- nlminb(c(median(D$SLV),0.3), logLnorm,lower=c(-Inf,0), y=D$SLV)

# Illustrate optimal parameters for the Cauchy model fit and Normal
hist(D$SLV, breaks = 50, freq=FALSE,ylim=c(0,11.5))
lines(seqN, dcauchy(seqN, opt_cauchy$par[1], opt_cauchy$par[2]), col = 'blue')
lines(seqN, dnorm(seqN, opt$par[1], opt$par[2]), col = 'red') # NORMAL FIT
legend("topright", legend = c("Cauchy", "Normal"), col = c("blue", "red"), lty=c(1,1))

# AIC
-2*(-opt_cauchy$objective) + 2*2 # Lav er fedt: Cauchy vinder
-2*(-opt$objective) + 2*2

# WALD Confidence Intervals
library(numDeriv)
H <- hessian(logLnorm, y=D$SLV, opt$par)
se.beta <- sqrt(diag(solve(H)))

WALDSslv = rbind(opt$par, opt$par - qnorm(1-0.05/2) * se.beta, opt$par + qnorm(1-0.05/2) * se.beta)
colnames(WALDSslv) = c("µ", "sd")
rownames(WALDSslv) = c("Estimate", "CI-lower", "CI-upper")
t(WALDSslv)
    # GLM: Normal w/ over-dispersion?


             
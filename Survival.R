# I. Assignment, 2. Theme: Survival
setwd("~/Documents/DTU/1. Semester (MSc)/Statistical Modelling/Assignments/Data")

# 1. Read
D2 = read.table('Logistic.txt', header = TRUE)
D3 = read.table('actg320.txt', header = TRUE)

# 2. Fit binomial to all
AIDS = sum(D2$AIDS_yes)
healthy = sum(D2$n)

k = c(0,1) # 1 svarer til AIDS-TRUE
nk = c(healthy, AIDS)

logL1b = function(theta){
  -sum(dbinom(k, size=1, prob = theta, log=TRUE) * nk) # Neg. logL, s? optimiser kan spise den. 
                            # Optimiser minimerer - vi vil maksimerer - s? vi tager siger: MIN -logL = max logL
}

logL1 = function(theta){
  -dbinom(69, size=sum(nk), prob = theta, log=TRUE) # Neg. logL, s? optimiser kan spise den. 
  # Optimiser minimerer - vi vil maksimerer - s? vi tager siger: MIN -logL = max logL
}


optimise(logL1, c(0,1))$minimum # Theta ligger mellem 0 eller 1

# Kontrol - empirisk theta:
AIDS / (healthy+AIDS)


# 3. Fit binomial separately

# patients treated with AZT
yAZT_AIDS = D2[1,2]
yAZT_healthy = D2$n[1] - D2[1,2] 

nk2 = c(yAZT_healthy, yAZT_AIDS)

logL2 = function(theta){
  -sum(dbinom(k, size=1, prob = theta, log=TRUE) * nk2) # Neg. logL, s? optimiser kan spise den. 
  # Optimiser minimerer - vi vil maksimerer - s? vi tager siger: MIN -logL = max logL
}
optimise(logL2, c(0,1))$minimum # Theta ligger mellem 0 eller 1

# Kontrol - empirisk theta:
yAZT_AIDS / (yAZT_healthy+yAZT_AIDS)





#patients not treated with AZT
nAZT_AIDS = D2[2,2]
nAZT_healthy = D2[2,3] - D2[2,2]

nk3 = c(nAZT_healthy, nAZT_AIDS)

logL3 = function(theta){
  -sum(dbinom(k, size=1, prob = theta, log=TRUE) * nk3) # Neg. logL, s? optimiser kan spise den. 
  # Optimiser minimerer - vi vil maksimerer - s? vi tager siger: MIN -logL = max logL
}
optimise(logL3, c(0,1))$minimum # Theta ligger mellem 0 eller 1

# Kontrol - empirisk theta:
44 / 168


#compare patients treated with AZT to patients not treated with AZT
prop.test(c(25,44), c(170, 168))
  # SIGNIFICANT IMPACT OF THE TREATMENT




#### Davids noter
# 4. Estimate the parameters in the model

# For p0 (no treatment):
logL1p0 = function(ß0){
  p0 = exp(ß0) / (1 + exp(ß0))
  -dbinom(nk3[2], size=sum(nk3), prob = p0, log=TRUE) # Neg. logL, s? optimiser kan spise den. 
  # Optimiser minimerer - vi vil maksimerer - saa vi siger: MIN -logL = max logL
}
ß0_opt = nlminb(c(0), logL1p0, lower = c(-Inf), upper = c(Inf))$par
ß0_opt

# Kontrol:
exp(ß0_opt) / (1 + exp(ß0_opt))
    # FEDT! Bemærk: Høj ß0 = høj ß0, lav (negativ) ß0 = lav p | ß0 = [-Inf, Inf]

#### For p1:
logL1p1 = function(ß1){
  p1 = exp(ß0_opt + ß1) / (1 + exp(ß0_opt + ß1))
  -dbinom(nk2[2], size=sum(nk2), prob = p1, log=TRUE) # Neg. logL, s? optimiser kan spise den. 
# Optimiser minimerer - vi vil maksimerer - saa vi siger: MIN -logL = max logL
}
ß1_opt = nlminb(c(0), logL1p1, lower = c(-Inf), upper = c(Inf))$par
ß1_opt

# Kontrol:
exp(ß0_opt + ß1_opt) / (1 + exp(ß0_opt + ß1_opt))

##### Confidence interval for ß1:
# If ß1 is not significantly different from 0 (0 is within the interval), then
# there is no effect from the treatment (no constant ß1 added to the probability
  # of getting AIDS)

# Ved insertion af ß0 = ß0_opt har vi profile likelihood'en for ß1
# Vi kan da bestemme likelihood-baseret konfidensinterval:
seq_ß1 = seq(-3,3,0.01)
l_ß1_vals = c()
for (theta in seq_ß1){
  # Profile logLikelihood as a function of lambda
  l_ß1_vals = c(l_ß1_vals, logL1p1(theta))
}
# Illustrate CI's
plot(seq_ß1, exp(-l_ß1_vals - max(-l_ß1_vals)), 'l', ylim=c(0,1), xlim = c(-1.5,0.5)) # Normalisering i logL: /maxL() => -maxL()  . Opløft den til e for at nå tilbage til ikke log-data
lines(seq_ß1, rep(exp(-1/2 * qchisq(1-0.05,1)), length(seq_ß1)), col=2)
  # I.e. CI for ß1 = [-1.2, -0.3].
  # Dvs. 0 er ikke inkluderet <=> treatment (ß1) har en signifikant effekt (!)




########################
########################
########################
########################
### Survival part 2
head(D3)
summary(D3)
hist(D3$time)
AIDS = D3$event == TRUE
lifetimeAIDS = D3$time[AIDS]
lifetimeCENSORED = D3$time[D3$event == 0]# Censored data, all that didn't die or get AIDS during the study


#### 2. AIDS statistics
mean(D3$event[D3$tx == 1]) # Perc. of AIDS on treatment
mean(D3$event[D3$tx == 0]) # Perc. of AIDS on no-treatment

# Lifetime of AIDS-patients on treatment
hist(D3$time[D3$tx == 1 & AIDS],   freq=FALSE, ylim=c(0,0.01)) # Lifetime of AIDS-patients on treatment
# Lifetime of AIDS-patients NOT on treatment
hist(D3$time[D3$tx == 0 & AIDS],   freq=FALSE, ylim=c(0,0.01))
    # Generelt ser AIDS-patienter uden treatment ud til at leve længere

# Illustrate lifetime for AIDS patients
hist(lifetimeAIDS)

######## 
### 3. Fit exponential to

### a) Using all data
# L(theta) = ∏ f(t_j)  * ∏ 1 - F(t_i)
# L(theta) = ∏ Sands. for at leve til t_j (AIDS)    ∏ Sands for at leve længere end t_i (CENSORED) 
nLogL1 = function(theta){
  - (sum(dexp(lifetimeAIDS, rate = theta, log=TRUE)) + sum( log(1 - pexp(lifetimeCENSORED, rate = theta)) ))
}
lambda1 = nlminb(c(1 / mean(D3$time)), nLogL1, lower = c(10^(-10)), upper = c(Inf))$par
lambda1

### Lifetime seperately for the treatment groups
# treatment
nLogLtreat = function(theta){
  - (sum(dexp(D3$time[D3$tx == 1 & AIDS] , rate = theta, log=TRUE)) + sum( log(1 - pexp(D3$time[D3$tx == 1 & !AIDS], rate = theta)) ))
}
lambda_treat = nlminb(c(1 / mean(D3$time)), nLogLtreat, lower = c(10^(-10)), upper = c(Inf))$par
lambda_treat

# NO treatment
nLogLNOtreat = function(theta){
  - (sum(dexp(D3$time[D3$tx == 0 & AIDS] , rate = theta, log=TRUE)) + sum( log(1 - pexp(D3$time[D3$tx == 0 & !AIDS], rate = theta)) ))
}
lambda_NOtreat = nlminb(c(1 / mean(D3$time)), nLogLNOtreat, lower = c(10^(-10)), upper = c(Inf))$par
lambda_NOtreat
  # Dvs. lambda er væsentligt lavere for treatment-levetiderne <=> færre (forventede) AIDS-tilfælde.


############# 
### 4. Comparison of likelihood
# IGEN Likelihood Ratio Test, fordi modellerne IKKE er nested (?) i.e. vi kan ikke sætte
# koefficienter = 0 og opnå en anden en anden - fordi de er på forskellig data.

# AIC-test (lav er fedest):
# I rækkefølge: Exp-fit af 1. al data, 2. treatment data, 3. non-treatment data (AIC)
-2*(-nlminb(c(1 / mean(D3$time)), nLogL1, lower = c(10^(-10)), upper = c(Inf))$objective) + 2*1
-2*(-nlminb(c(1 / mean(D3$time)), nLogLtreat, lower = c(10^(-10)), upper = c(Inf))$objective) + 2*1
-2*(-nlminb(c(1 / mean(D3$time)), nLogLNOtreat, lower = c(10^(-10)), upper = c(Inf))$objective) + 2*1
    # Dvs. Exp-fit af TREATMENT DATA er bedst.




############ 
### 5. Formulate model and calc. MLE
# SE NOTER: E(T) = exp(ß0) = 1 / lambda0 (jvf. formel)    => lambda0 = 1 / exp(ß0)
# Tilsvarende:                                              lambda1 = 1 / exp(ß0 + ß1)
# 4. Estimate the parameters in the model

# For p0 (no treatment):
logL_notreatß0 = function(ß0){
  lambda0 = 1 / exp(ß0)
  - (sum(dexp(D3$time[D3$tx == 0 & AIDS] , rate = lambda0, log=TRUE)) + sum( log(1 - pexp(D3$time[D3$tx == 0 & !AIDS], rate = lambda0)) ))
}
ß0_opt = nlminb(c(10), logL_notreatß0, lower = c(-Inf), upper = c(Inf))$par
ß0_opt

# Kontrol:
1/exp(ß0_opt)
lambda_NOtreat
  # FEDT!

#### For p1 (treatment):
logL_treatß1 = function(ß1){
  lambda1 = 1 / exp(ß0_opt + ß1)
  - (sum(dexp(D3$time[D3$tx == 1 & AIDS] , rate = lambda1, log=TRUE)) + sum( log(1 - pexp(D3$time[D3$tx == 1 & !AIDS], rate = lambda1)) ))
  # Optimiser minimerer - vi vil maksimerer - saa vi siger: MIN -logL = max logL
}
ß1_opt = nlminb(c(0), logL_treatß1, lower = c(-Inf), upper = c(Inf))$par
ß1_opt

# Kontrol:
1 / exp(ß0_opt + ß1_opt)
lambda_treat
  # Fedt! Meget lille ß1, mon der er en treatment-effekt?

############ 
### 6. Wald Confidence Intervals
# Fra L(ß1), calc. hessian => sigma_ß1 => CI's
library(numDeriv)
H <- hessian(logL_treatß1, ß1_opt)
se.beta <- sqrt(diag(solve(H)))

WALDß1 = rbind(ß1_opt, ß1_opt - qnorm(1-0.05/2) * se.beta, ß1_opt + qnorm(1-0.05/2) * se.beta)
colnames(WALDß1) = c("ß1")
rownames(WALDß1) = c("Estimate", "CI-lower", "CI-upper")
t(WALDß1)
  # Omend en lille, så er treatment-effekten signifikant (forskellig fra ß1 = 0)


############
############ 
### 7. Derive theoretical results


# Ved insertion af ß0 = ß0_opt har vi profile likelihood'en for ß1
# Vi kan da bestemme likelihood-baseret konfidensinterval:
seq_ß1 = seq(0.01,1.3,0.01)
l_ß1_vals = c()
for (theta in seq_ß1){
  # Profile logLikelihood as a function of lambda
  l_ß1_vals = c(l_ß1_vals, logL_treatß1(theta))
}
# Illustrate CI's
plot(seq_ß1, exp(-l_ß1_vals - max(-l_ß1_vals)), 'l', ylim=c(0,1)) # Normalisering i logL: /maxL() => -maxL()  . Opløft den til e for at nå tilbage til ikke log-data
lines(seq_ß1, rep(exp(-1/2 * qchisq(1-0.05,1)), length(seq_ß1)), col=2)
# Dvs. 0 er ikke inkluderet <=> treatment (ß1) har en signifikant effekt (!)

# MANGLER: Deriving therortical results
